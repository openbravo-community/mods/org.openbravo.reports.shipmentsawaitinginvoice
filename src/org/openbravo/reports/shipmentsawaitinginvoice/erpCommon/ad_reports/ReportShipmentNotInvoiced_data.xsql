<?xml version="1.0" encoding="UTF-8" ?>
<!--
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2009 Openbravo SLU 
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
-->

<SqlClass name="ReportShipmentNotInvoicedData" package="org.openbravo.reports.shipmentsawaitinginvoice.erpCommon.ad_reports">
  <SqlClassComment></SqlClassComment>
  <SqlMethod name="select" type="preparedStatement" return="multiple">
    <SqlMethodComment></SqlMethodComment>
    <Sql>
    <![CDATA[
    SELECT ORGNAME, BPNAME, M_INOUT_ID, DOCUMENTNO, DATEORDERED, MOVEMENTDATE, DOCSTATUS, SHIPCOMP, 
    GRANDTOTAL, PAYMENTFORM,  PAYMENTTERM, DELIVERYLOCATION,     
	LINE, PRODNAME, SOLDQTY, SHIPPEDQTY, INVOICEDQTY, UOM, PRICE, 
	AMOUNT, TAX, TAXBASE, AMOUNTPENDINGINVOICE,C_CURRENCY_SYMBOL(?, 0, 'Y') AS CURRENCY
	FROM (
		SELECT ORG.NAME AS ORGNAME, BP.NAME AS BPNAME, SP.M_INOUT_ID AS M_INOUT_ID, TO_DATE(SP.DATEORDERED) AS DATEORDERED,
	    SP.DOCUMENTNO AS DOCUMENTNO, TO_DATE(SP.MOVEMENTDATE) AS MOVEMENTDATE, REFLISTDOC.NAME AS DOCSTATUS, SH.NAME AS SHIPCOMP,
	    REFLISTV.NAME AS PAYMENTFORM, PT.NAME AS PAYMENTTERM, COALESCE(DLOC.NAME, BPADD.NAME) AS DELIVERYLOCATION,
	    PR.NAME || ' ' || COALESCE(TO_CHAR(PR.DESCRIPTION), '') AS PRODNAME, OL.LINE AS LINE, 
        C_CURRENCY_CONVERT(O.GRANDTOTAL, COALESCE(OL.C_CURRENCY_ID, O.C_CURRENCY_ID), ?, TO_DATE(COALESCE(OL.DATEORDERED, O.DATEORDERED, NOW())), NULL, OL.AD_CLIENT_ID, OL.AD_ORG_ID) AS GRANDTOTAL,
        OL.QTYORDERED AS SOLDQTY, OL.QTYDELIVERED AS SHIPPEDQTY, OL.QTYINVOICED AS INVOICEDQTY,
	    COALESCE(TO_CHAR(UO.UOMSYMBOL), '') AS UOM,
        C_CURRENCY_CONVERT(OL.PRICEACTUAL, COALESCE(OL.C_CURRENCY_ID, O.C_CURRENCY_ID), ?, TO_DATE(COALESCE(OL.DATEORDERED, O.DATEORDERED, NOW())), NULL, OL.AD_CLIENT_ID, OL.AD_ORG_ID) AS PRICE,
        C_CURRENCY_CONVERT(OL.LINENETAMT, COALESCE(OL.C_CURRENCY_ID, O.C_CURRENCY_ID), ?, TO_DATE(COALESCE(OL.DATEORDERED, O.DATEORDERED, NOW())), NULL, OL.AD_CLIENT_ID, OL.AD_ORG_ID) AS AMOUNT,
	    NULL AS TAX, NULL AS TAXBASE,
        C_CURRENCY_CONVERT(((OL.QTYDELIVERED-OL.QTYINVOICED)*OL.PRICEACTUAL), COALESCE(OL.C_CURRENCY_ID, O.C_CURRENCY_ID), ?, TO_DATE(COALESCE(OL.DATEORDERED, O.DATEORDERED, NOW())), NULL, OL.AD_CLIENT_ID, OL.AD_ORG_ID) AS AMOUNTPENDINGINVOICE
	    FROM 
	    M_INOUT SP LEFT JOIN C_ORDER O
	    ON SP.C_ORDER_ID = O.C_ORDER_ID
	    LEFT JOIN M_SHIPPER SH
	    ON SP.M_SHIPPER_ID = SH.M_SHIPPER_ID
	    LEFT JOIN AD_REF_LIST_V REFLISTV
	    ON (REFLISTV.VALUE = O.PAYMENTRULE AND REFLISTV.AD_LANGUAGE = ?)
	    LEFT JOIN AD_REF_LIST_V REFLISTDOC
	    ON (REFLISTDOC.VALUE = SP.DOCSTATUS AND REFLISTDOC.AD_LANGUAGE = ?)
	    LEFT JOIN C_PAYMENTTERM PT
	    ON O.C_PAYMENTTERM_ID = PT.C_PAYMENTTERM_ID
	    LEFT JOIN C_BPARTNER_LOCATION DLOC
	    ON DLOC.C_BPARTNER_LOCATION_ID = O.DELIVERY_LOCATION_ID,
	    C_ORDERLINE OL LEFT JOIN M_PRODUCT PR
	    ON OL.M_PRODUCT_ID = PR.M_PRODUCT_ID,
	    AD_ORG ORG, C_BPARTNER BP, C_UOM UO, C_BPARTNER_LOCATION BPADD
	      WHERE SP.C_ORDER_ID=O.C_ORDER_ID
	      AND O.C_ORDER_ID=OL.C_ORDER_ID
	      AND SP.C_BPARTNER_ID = BP.C_BPARTNER_ID
          AND O.C_BPARTNER_LOCATION_ID = BPADD.C_BPARTNER_LOCATION_ID
	      AND O.PAYMENTRULE = REFLISTV.VALUE
	      AND OL.C_UOM_ID = UO.C_UOM_ID
	      AND SP.AD_ORG_ID = ORG.AD_ORG_ID
	      AND REFLISTV.AD_REFERENCE_ID = '195'
	      AND REFLISTDOC.AD_REFERENCE_ID = '131'
	      AND O.PROCESSED = 'Y'
	      AND O.ISSOTRX = 'Y'
	      AND OL.QTYDELIVERED<>OL.QTYINVOICED 
	      AND SP.AD_Client_ID IN ('1')
	      AND SP.AD_ORG_ID IN ('1')
	      AND 1=1
	    UNION ALL
	 	SELECT ORG1.NAME AS ORGNAME, BP1.NAME AS BPNAME, SP1.M_INOUT_ID AS M_INOUT_ID, TO_DATE(SP1.DATEORDERED) AS DATEORDERED,
	    SP1.DOCUMENTNO AS DOCUMENTNO, TO_DATE(SP1.MOVEMENTDATE) AS MOVEMENTDATE, REFLISTDOC1.NAME AS DOCSTATUS, SH1.NAME AS SHIPCOMP,
	    REFLISTV1.NAME AS PAYMENTFORM, PT1.NAME AS PAYMENTTERM, COALESCE(DLOC1.NAME, BPADD1.NAME) AS DELIVERYLOCATION,
	    T1.NAME AS PRODNAME, NULL AS LINE, 
        C_CURRENCY_CONVERT(O1.GRANDTOTAL, O1.C_CURRENCY_ID, ?, TO_DATE(COALESCE(O1.DATEORDERED, NOW())), NULL, O1.AD_CLIENT_ID, O1.AD_ORG_ID) AS GRANDTOTAL,
	    NULL AS SOLDQTY, NULL AS SHIPPEDQTY, NULL AS INVOICEDQTY,
	    NULL AS UOM,
	    NULL AS PRICE, 
        C_CURRENCY_CONVERT(OT1.TAXAMT,  O1.C_CURRENCY_ID, ?, TO_DATE(COALESCE(O1.DATEORDERED, NOW())), NULL, OT1.AD_CLIENT_ID, OT1.AD_ORG_ID) AS AMOUNT,
        T1.RATE AS TAX,  
        C_CURRENCY_CONVERT(OT1.TAXBASEAMT, O1.C_CURRENCY_ID, ?, TO_DATE(COALESCE(O1.DATEORDERED, NOW())), NULL, OT1.AD_CLIENT_ID, OT1.AD_ORG_ID) AS TAXBASE,
        C_CURRENCY_CONVERT(OT1.TAXAMT, O1.C_CURRENCY_ID, ?, TO_DATE(COALESCE(O1.DATEORDERED, NOW())), NULL, OT1.AD_CLIENT_ID, OT1.AD_ORG_ID) AS AMOUNTPENDINGINVOICE
	    FROM 
	    M_INOUT SP1 LEFT JOIN C_ORDER O1
	    ON SP1.C_ORDER_ID = O1.C_ORDER_ID
	    LEFT JOIN C_ORDERTAX OT1
	    ON O1.C_ORDER_ID = OT1.C_ORDER_ID
	    LEFT JOIN C_TAX T1
	    ON OT1.C_TAX_ID = T1.C_TAX_ID
	    LEFT JOIN M_SHIPPER SH1
	    ON SP1.M_SHIPPER_ID = SH1.M_SHIPPER_ID
	    LEFT JOIN AD_REF_LIST_V REFLISTV1
	    ON (REFLISTV1.VALUE = O1.PAYMENTRULE AND REFLISTV1.AD_LANGUAGE = ?)
	    LEFT JOIN AD_REF_LIST_V REFLISTDOC1
	    ON (REFLISTDOC1.VALUE = SP1.DOCSTATUS AND REFLISTDOC1.AD_LANGUAGE = ?)
	    LEFT JOIN C_PAYMENTTERM PT1
	    ON O1.C_PAYMENTTERM_ID = PT1.C_PAYMENTTERM_ID
	   	LEFT JOIN C_BPARTNER_LOCATION DLOC1
	    ON DLOC1.C_BPARTNER_LOCATION_ID = O1.DELIVERY_LOCATION_ID,
	    AD_ORG ORG1, C_BPARTNER BP1,C_BPARTNER_LOCATION BPADD1
	      WHERE SP1.C_ORDER_ID=O1.C_ORDER_ID
	      AND O1.C_ORDER_ID=OT1.C_ORDER_ID
	      AND OT1.C_TAX_ID = T1.C_TAX_ID
	      AND SP1.C_BPARTNER_ID = BP1.C_BPARTNER_ID
          AND O1.C_BPARTNER_LOCATION_ID = BPADD1.C_BPARTNER_LOCATION_ID
	      AND O1.PAYMENTRULE = REFLISTV1.VALUE
	      AND SP1.AD_ORG_ID = ORG1.AD_ORG_ID
	      AND REFLISTV1.AD_REFERENCE_ID = '195'
	      AND REFLISTDOC1.AD_REFERENCE_ID = '131'
	      AND O1.PROCESSED = 'Y'
	      AND O1.ISSOTRX = 'Y'
	      AND EXISTS (SELECT 1 FROM C_ORDER C2, C_ORDERLINE CL
          	WHERE C2.C_ORDER_ID = O1.C_ORDER_ID
         	 AND C2.C_ORDER_ID=CL.C_ORDER_ID
         	 AND CL.QTYDELIVERED<>CL.QTYINVOICED)
	      AND SP1.AD_Client_ID IN ('2')
	      AND SP1.AD_ORG_ID IN ('2')
	      AND 2=2) AAA     
	ORDER BY ORGNAME, BPNAME, MOVEMENTDATE DESC, DOCUMENTNO, LINE
      ]]>
     </Sql>

    <Parameter name="cCurrencyConv"/>
    <Parameter name="cCurrencyConv"/>
    <Parameter name="cCurrencyConv"/>
    <Parameter name="cCurrencyConv"/>
    <Parameter name="cCurrencyConv"/>
    <Parameter name="adLanguage"/>
    <Parameter name="adLanguage"/>
    <Parameter name="adUserClient" type="replace" optional="true" after="SP.AD_Client_ID IN (" text="'1'"/>
    <Parameter name="adUserOrg" type="replace" optional="true" after="SP.AD_ORG_ID IN (" text="'1'"/>
    <Parameter name="cBpartnerId" optional="true" after="1=1">AND SP.C_BPARTNER_ID = ?</Parameter>
    <Parameter name="cOrgId" optional="true" after="1=1">AND SP.AD_ORG_ID = ?</Parameter>
    <Parameter name="dateFrom" optional="true" after="1=1"><![CDATA[ AND SP.MOVEMENTDATE >= to_date(?) ]]></Parameter>
    <Parameter name="dateTo" optional="true" after="1=1"><![CDATA[ AND SP.MOVEMENTDATE < to_date(?) ]]></Parameter>
    <Parameter name="orderDocNo" optional="true" after="1=1"><![CDATA[AND UPPER(O.DOCUMENTNO) LIKE '%'||UPPER(?)||'%']]></Parameter>
    <Parameter name="shipmentDocNo" optional="true" after="1=1"><![CDATA[AND UPPER(SP.DOCUMENTNO) LIKE '%'||UPPER(?)||'%']]></Parameter>
    <Parameter name="deliveryTerm" optional="true" after="1=1"><![CDATA[ AND O.DELIVERYRULE = ? ]]></Parameter>
    <Parameter name="paymentTerm" optional="true" after="1=1"><![CDATA[ AND PT.C_PaymentTerm_ID = ? ]]></Parameter>
    <Parameter name="paymentForm" optional="true" after="1=1"><![CDATA[ AND O.PAYMENTRULE = ? ]]></Parameter>
     
    <Parameter name="cCurrencyConv"/>
    <Parameter name="cCurrencyConv"/>
    <Parameter name="cCurrencyConv"/>
    <Parameter name="cCurrencyConv"/>
    <Parameter name="adLanguage"/>
    <Parameter name="adLanguage"/>    
    <Parameter name="adUserClient" type="replace" optional="true" after="SP1.AD_Client_ID IN (" text="'2'"/>
    <Parameter name="adUserOrg" type="replace" optional="true" after="SP1.AD_ORG_ID IN (" text="'2'"/>
    <Parameter name="cBpartnerId" optional="true" after="2=2">AND SP1.C_BPARTNER_ID = ?</Parameter>
    <Parameter name="cOrgId" optional="true" after="2=2">AND SP1.AD_ORG_ID = ?</Parameter>
    <Parameter name="dateFrom" optional="true" after="2=2"><![CDATA[ AND SP1.MOVEMENTDATE >= to_date(?) ]]></Parameter>
    <Parameter name="dateTo" optional="true" after="2=2"><![CDATA[ AND SP1.MOVEMENTDATE < to_date(?) ]]></Parameter>
    <Parameter name="orderDocNo" optional="true" after="2=2"><![CDATA[AND UPPER(O1.DOCUMENTNO) LIKE '%'||UPPER(?)||'%']]></Parameter>
    <Parameter name="shipmentDocNo" optional="true" after="2=2"><![CDATA[AND UPPER(SP1.DOCUMENTNO) LIKE '%'||UPPER(?)||'%']]></Parameter>
    <Parameter name="deliveryTerm" optional="true" after="2=2"><![CDATA[ AND O1.DELIVERYRULE = ? ]]></Parameter>
    <Parameter name="paymentTerm" optional="true" after="2=2"><![CDATA[ AND PT1.C_PaymentTerm_ID = ? ]]></Parameter>
    <Parameter name="paymentForm" optional="true" after="2=2"><![CDATA[ AND O1.PAYMENTRULE = ? ]]></Parameter>
      
  </SqlMethod>
  
  <SqlMethod name="set" type="constant" return="multiple">
      <SqlMethodComment></SqlMethodComment>
      <Sql></Sql>
  </SqlMethod>
  
  <SqlMethod name="bPartnerDescription" type="preparedStatement" return="String" default="">
    <SqlMethodComment></SqlMethodComment>
    <Sql>
    SELECT MAX(NAME) AS name FROM C_BPARTNER 
    WHERE C_BPARTNER_ID = ?
    </Sql>
    <Parameter name="cBpartnerId"/>
  </SqlMethod>
</SqlClass>