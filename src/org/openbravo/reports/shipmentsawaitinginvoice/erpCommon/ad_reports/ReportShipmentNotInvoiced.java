/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2009 Openbravo SLU 
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package org.openbravo.reports.shipmentsawaitinginvoice.erpCommon.ad_reports;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.erpCommon.ad_combos.OrganizationComboData;
import org.openbravo.erpCommon.businessUtility.WindowTabs;
import org.openbravo.erpCommon.utility.ComboTableData;
import org.openbravo.erpCommon.utility.LeftTabsBar;
import org.openbravo.erpCommon.utility.NavigationBar;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.ToolBar;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.xmlEngine.XmlDocument;

public class ReportShipmentNotInvoiced extends HttpSecureAppServlet {
  private static final long serialVersionUID = 1L;

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);
    String strUserCurrencyId = Utility.stringBaseCurrencyId(this, vars.getClient());
    if (vars.commandIn("DEFAULT")) {
      String strDateFrom = vars.getGlobalVariable("inpDateFrom",
          "ReportShipmentNotInvoiced|dateFrom", "");
      String strDateTo = vars
          .getGlobalVariable("inpDateTo", "ReportShipmentNotInvoiced|dateTo", "");
      String strCBpartnerId = vars.getGlobalVariable("inpcBPartnerId",
          "ReportShipmentNotInvoiced|bpartner", "");
      String strCOrgId = vars.getGlobalVariable("inpOrg", "ReportShipmentNotInvoiced|Org", "");
      String strOrderDocNo = vars.getGlobalVariable("inpOrderDocNo",
          "ReportShipmentNotInvoiced|orderDocNo", "");
      String strDeliveryTerms = vars.getGlobalVariable("inpDeliveryTerms",
          "ReportShipmentNotInvoiced|deliveryTerms", "");
      String strTermPayment = vars.getGlobalVariable("inppaymentterm",
          "ReportShipmentNotInvoiced|paymentterm", "");

      String strPaymentTerms = vars.getGlobalVariable("inpCPaymentRuleId",
          "ReportShipmentNotInvoiced|PaymentRule", "");
      String strOrderRef = vars.getGlobalVariable("inpOrderRef",
          "ReportShipmentNotInvoiced|orderRef", "");
      String strCurrencyId = vars.getGlobalVariable("inpCurrencyId",
          "ReportShipmentNotInvoiced|currency", strUserCurrencyId);
      String strShipmentDocNo = vars.getGlobalVariable("inpShipmentDocNo",
          "ReportShipmentNotInvoiced|shipmentDocNo", "");
      printPageDataSheet(response, vars, strDateFrom, strDateTo, strCBpartnerId, strDeliveryTerms,
          strOrderDocNo, strOrderRef, strCOrgId, strCurrencyId, strPaymentTerms, strTermPayment,
          strShipmentDocNo);
    } else if (vars.commandIn("FIND_HTML", "FIND_PDF")) {
      String strDateFrom = vars.getRequestGlobalVariable("inpDateFrom",
          "ReportShipmentNotInvoiced|dateFrom");
      String strDateTo = vars.getRequestGlobalVariable("inpDateTo",
          "ReportShipmentNotInvoiced|dateTo");
      String strCBpartnerId = vars.getRequestGlobalVariable("inpcBPartnerId",
          "ReportShipmentNotInvoiced|bpartner");
      String strDeliveryTerms = vars.getRequestGlobalVariable("inpDeliveryTerms",
          "ReportShipmentNotInvoiced|deliveryTerms");
      String strOrderDocNo = vars.getRequestGlobalVariable("inpOrderDocNo",
          "ReportShipmentNotInvoiced|orderDocNo");
      String strOrderRef = vars.getRequestGlobalVariable("inpOrderRef",
          "ReportShipmentNotInvoiced|orderRef");
      String strCOrgId = vars.getRequestGlobalVariable("inpOrg", "ReportShipmentNotInvoiced|Org");
      String strOutput = "html";
      String strShipmentDocNo = vars.getRequestGlobalVariable("inpShipmentDocNo",
          "ReportShipmentNotInvoiced|shipmentDocNo");
      String strPaymentForm = vars.getRequestGlobalVariable("inpCPaymentRuleId",
          "ReportShipmentNotInvoiced|PaymentRule");
      String strTermPayment = vars.getRequestGlobalVariable("inppaymentterm",
          "ReportShipmentNotInvoiced|paymentterm");
      String strCurrencyId = vars.getRequestGlobalVariable("inpCurrencyId",
          "ReportShipmentNotInvoiced|currency");
      if (vars.commandIn("FIND_PDF"))
        strOutput = "pdf";
      printPage(request, response, vars, strDateFrom, strDateTo, strCBpartnerId, strDeliveryTerms,
          strOrderDocNo, strOrderRef, strCOrgId, strOutput, strShipmentDocNo, strPaymentForm,
          strTermPayment, strCurrencyId);
    } else
      pageError(response);
  }

  private void printPageDataSheet(HttpServletResponse response, VariablesSecureApp vars,
      String strdateFrom, String strdateTo, String strcBpartnerId, String strDeliveryTerms,
      String strOrderDocNo, String strOrderRef, String strCOrgId, String strCurrencyId,
      String strPaymentForm, String strTermPayment, String strShipmentDocNo) throws IOException,
      ServletException {
    if (log4j.isDebugEnabled())
      log4j.debug("Output: dataSheet");
    XmlDocument xmlDocument = null;
    xmlDocument = xmlEngine
        .readXmlTemplate(
            "org/openbravo/reports/shipmentsawaitinginvoice/erpCommon/ad_reports/ReportShipmentNotInvoiced")
        .createXmlDocument();

    ToolBar toolbar = new ToolBar(this, vars.getLanguage(), "ReportShipmentNotInvoiced", false, "",
        "", "", false, "ad_reports", strReplaceWith, false, true);
    toolbar.prepareSimpleToolBarTemplate();
    xmlDocument.setParameter("toolbar", toolbar.toString());
    try {
      WindowTabs tabs = new WindowTabs(this, vars,
          "org.openbravo.reports.shipmentsawaitinginvoice.erpCommon.ad_reports.ReportShipmentNotInvoiced");
      xmlDocument.setParameter("parentTabContainer", tabs.parentTabs());
      xmlDocument.setParameter("mainTabContainer", tabs.mainTabs());
      xmlDocument.setParameter("childTabContainer", tabs.childTabs());
      xmlDocument.setParameter("theme", vars.getTheme());
      NavigationBar nav = new NavigationBar(this, vars.getLanguage(),
          "ReportShipmentNotInvoiced.html", classInfo.id, classInfo.type, strReplaceWith, tabs
              .breadcrumb());
      xmlDocument.setParameter("navigationBar", nav.toString());
      LeftTabsBar lBar = new LeftTabsBar(this, vars.getLanguage(),
          "ReportShipmentNotInvoiced.html", strReplaceWith);
      xmlDocument.setParameter("leftTabs", lBar.manualTemplate());
    } catch (Exception ex) {
      throw new ServletException(ex);
    }

    OBError myMessage = vars.getMessage("ReportShipmentNotInvoiced");
    vars.removeMessage("ReportShipmentNotInvoiced");
    if (myMessage != null) {
      xmlDocument.setParameter("messageType", myMessage.getType());
      xmlDocument.setParameter("messageTitle", myMessage.getTitle());
      xmlDocument.setParameter("messageMessage", myMessage.getMessage());
    }
    xmlDocument.setParameter("calendar", vars.getLanguage().substring(0, 2));
    xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
    xmlDocument.setParameter("paramLanguage", "defaultLang=\"" + vars.getLanguage() + "\";");
    xmlDocument.setParameter("dateFrom", strdateFrom);
    xmlDocument.setParameter("dateFromdisplayFormat", vars.getSessionValue("#AD_SqlDateFormat"));
    xmlDocument.setParameter("dateFromsaveFormat", vars.getSessionValue("#AD_SqlDateFormat"));
    xmlDocument.setParameter("dateTo", strdateTo);
    xmlDocument.setParameter("dateTodisplayFormat", vars.getSessionValue("#AD_SqlDateFormat"));
    xmlDocument.setParameter("dateTosaveFormat", vars.getSessionValue("#AD_SqlDateFormat"));
    xmlDocument.setParameter("paramBPartnerId", strcBpartnerId);
    xmlDocument.setParameter("paramBPartnerDescription", ReportShipmentNotInvoicedData
        .bPartnerDescription(this, strcBpartnerId));
    xmlDocument.setParameter("deliveryTerms", strDeliveryTerms);
    try {
      ComboTableData comboTableData = new ComboTableData(vars, this, "LIST", "",
          "C_Order DeliveryRule", "", Utility.getContext(this, vars, "#AccessibleOrgTree",
              "ReportShipmentNotInvoiced"), Utility.getContext(this, vars, "#User_Client",
              "ReportShipmentNotInvoiced"), 0);
      Utility.fillSQLParameters(this, vars, null, comboTableData, "ReportShipmentNotInvoiced",
          strDeliveryTerms);
      xmlDocument.setData("reportDeliveryTerms", "liststructure", comboTableData.select(false));
      comboTableData = null;

    } catch (Exception ex) {
      throw new ServletException(ex);
    }
    xmlDocument.setParameter("ccurrencyid", strCurrencyId);
    try {
      ComboTableData comboTableData = new ComboTableData(vars, this, "TABLEDIR", "C_Currency_ID",
          "", "",
          Utility.getContext(this, vars, "#AccessibleOrgTree", "ReportShipmentNotInvoiced"),
          Utility.getContext(this, vars, "#User_Client", "ReportShipmentNotInvoiced"), 0);
      Utility.fillSQLParameters(this, vars, null, comboTableData, "ReportShipmentNotInvoiced",
          strCurrencyId);
      xmlDocument.setData("reportC_Currency_ID", "liststructure", comboTableData.select(false));
      comboTableData = null;
    } catch (Exception ex) {
      throw new ServletException(ex);
    }
    xmlDocument.setParameter("paymentRule", strPaymentForm);
    try {
      ComboTableData comboTableData = new ComboTableData(vars, this, "LIST", "",
          "All_Payment Rule", "", Utility.getContext(this, vars, "#AccessibleOrgTree",
              "ReportDebtPayment"), Utility.getContext(this, vars, "#User_Client",
              "ReportDebtPayment"), 0);
      Utility.fillSQLParameters(this, vars, null, comboTableData, "ReportShipmentNotInvoiced",
          strPaymentForm);
      xmlDocument.setData("reportPaymentRule", "liststructure", comboTableData.select(false));
      comboTableData = null;
    } catch (Exception ex) {
      throw new ServletException(ex);
    }
    xmlDocument.setParameter("paymentTerm", strTermPayment);
    try {
      ComboTableData comboTableData = new ComboTableData(vars, this, "TABLEDIR",
          "C_PaymentTerm_ID", "", "", Utility.getContext(this, vars, "#AccessibleOrgTree",
              "ReportShipmentNotInvoiced"), Utility.getContext(this, vars, "#User_Client",
              "ReportShipmentNotInvoiced"), 0);
      Utility.fillSQLParameters(this, vars, null, comboTableData, "ReportShipmentNotInvoiced",
          strTermPayment);
      xmlDocument.setData("reportPaymentTerm", "liststructure", comboTableData.select(false));
      comboTableData = null;
    } catch (Exception ex) {
      throw new ServletException(ex);
    }
    xmlDocument.setParameter("orderDocNo", strOrderDocNo);
    xmlDocument.setParameter("paramShipmentDocNo", strShipmentDocNo);
    xmlDocument.setParameter("orderRef", strOrderRef);
    xmlDocument.setParameter("adOrgId", strCOrgId);
    xmlDocument.setData("reportAD_ORGID", "liststructure", OrganizationComboData.selectCombo(this,
        vars.getRole()));
    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }

  private void printPage(HttpServletRequest request, HttpServletResponse response,
      VariablesSecureApp vars, String strdateFrom, String strdateTo, String strcBpartnerId,
      String strDeliveryTerms, String strOrderDocNo, String strOrderRef, String strCOrgId,
      String strOutput, String strShipmentDocNo, String strPaymentForm, String strTermPayment,
      String strCurrencyId) throws IOException, ServletException {
    if (log4j.isDebugEnabled())
      log4j.debug("Output: print html");
    ReportShipmentNotInvoicedData[] data = null;
    String strConvRateErrorMsg = "";
    OBError myMessage = null;
    try {
      data = ReportShipmentNotInvoicedData.select(this, strCurrencyId, vars.getLanguage(), Utility
          .getContext(this, vars, "#User_Client", "ReportShipmentNotInvoiced"), Utility.getContext(
          this, vars, "#AccessibleOrgTree", "ReportShipmentNotInvoiced"), strcBpartnerId,
          strCOrgId, strdateFrom, strdateTo, strOrderDocNo, strShipmentDocNo, strDeliveryTerms,
          strTermPayment, strPaymentForm);
    } catch (ServletException ex) {
      myMessage = Utility.translateError(this, vars, vars.getLanguage(), ex.getMessage());
    }
    if (myMessage != null) {
      strConvRateErrorMsg = myMessage.getMessage();
    }
    if (!strConvRateErrorMsg.equals("") && strConvRateErrorMsg != null) {
      advisePopUp(request, response, "ERROR", Utility.messageBD(this, "NoConversionRateHeader",
          vars.getLanguage()), strConvRateErrorMsg);
    } else {
      String strReportName = "@basedesign@/org/openbravo/reports/shipmentsawaitinginvoice/erpCommon/ad_reports/ReportShipmentNotInvoiced.jrxml";
      if (strOutput.equals("pdf"))
        response.setHeader("Content-disposition", "inline; filename=ReportShipmentNotInvoiced.pdf");

      String strSubTitle = "";
      strSubTitle = Utility.messageBD(this, "From", vars.getLanguage()) + " " + strdateFrom + " "
          + Utility.messageBD(this, "To", vars.getLanguage()) + " " + strdateTo;

      HashMap<String, Object> parameters = new HashMap<String, Object>();
      parameters.put("REPORT_SUBTITLE", strSubTitle);
      renderJR(vars, response, strReportName, strOutput, parameters, data, null);
    }
  }

}
